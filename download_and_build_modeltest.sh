#!/usr/bin/env bash

DEPENDENCIES=(
    "autoconf"
    "automake"
    "libtool"
    "bison"
    "flex"
    "cmake"
)

PLATFORM=$(awk '/^ID=/' /etc/*-release | awk -F'=' '{ print tolower($2) }')
MODELTEST_FOLDER_NAME="modeltest"
MODELTEST_EXECUTABLE_PATH="$MODELTEST_FOLDER_NAME/bin/modeltest-ng"

function download_modeltest  {
    echo "Try to download modeltest"

    if [ ! -d ./modeltest ]; then
	git clone --recursive https://github.com/ddarriba/modeltest.git $MODELTEST_FOLDER_NAME
    else
	return 1
    fi
}

function install_dependencies {
    echo "Install dependencies"

    case $PLATFORM in
	*opensuse*)
	    sudo zypper ref && sudo zypper install -n "${DEPENDENCIES[@]}"
	    ;;
	*ubuntu|debian*)
	    sudo apt update && sudo apt install -y "${DEPENDENCIES[@]}" build-essential
	    ;;
	*)
	    echo "Unknown platform"
    esac
}

function build_modeltest {
    echo "Building modeltest"

    if [ -d ./build ]; then
	rm -Rf build
    fi
    mkdir build && cd build
    cmake ..
    make
    cd ..
}

function main {
    download_modeltest
    if [ $? -ne 0 ]; then
	echo "Modeltest folder already exist"
	exit 1
    fi

    cd modeltest
    install_dependencies
    if [ $? -ne 0 ]; then
	echo "Failed to install dependencies"
	exit 1
    fi

    build_modeltest
    if [ $? -ne 0 ]; then
	echo "Failed to build modeltest"
	exit 1
    fi
    cd ..

    if [ -f "$PWD/$MODELTEST_EXECUTABLE_PATH" ]; then
	echo -e "Build successfully\nexecutable found at: $PWD/$MODELTEST_EXECUTABLE_PATH"
    fi
}

main


