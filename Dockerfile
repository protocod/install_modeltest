FROM debian:buster-slim AS builder
WORKDIR /build_modeltest
RUN apt update \
    && apt install -y \
    autoconf \
    automake \
    libtool \
    bison \
    flex \
    cmake \
    build-essential \
    git \
    && git clone --recursive https://github.com/ddarriba/modeltest && cd modeltest \
    && mkdir build && cd build \
    && cmake .. && make

FROM debian:buster-slim
WORKDIR /root
COPY --from=builder /build_modeltest/modeltest/bin/modeltest-ng /bin
CMD ["modeltest-ng", "--help"]
