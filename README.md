# Why ?
Provide a little bash script to download and build modeltest-ng on GNU/Linux using cmake.

# Supported operating systems

|OS|version|tested|
|--|-------|------|
|Debian|10|OK|
|Ubuntu|20.04 LTS|OK|
|Ubuntu|20.10|Nope, but it should be okay|
|OpenSUSE|Tumbleweed|OK|

**You can also use Docker if your operating system is not supported.**

# Instructions
There are 2 ways to build and use modeltest.

1. Build directly modeltest on your computer using the `download_and_build_modeltest.sh` script.
2. or using docker to build an image and start a container which provide modeltest-ng binary file.

If you have installed docker on your computer, I suggest to use the Dockerfile instead of the `download_and_build_modeltest.sh` script.

## 1 - Build on your computer

Download `download_and_build_modeltest.sh` script and run it as simple user, **not root user** !

> The user account must be allowed to execute a package manager using sudo command.

## 2 - Or use the power of docker !

Build a docker image using:
```sh
docker build -t protocod/modeltest-ng .
```

Then, run a container using this image in interactive mode with:
```sh
docker run -it protocod/modeltest-ng bash
```

`modeltest-ng` will be in /bin folder. So you can type `modeltest-ng` command directly like this:

```sh
modeltest-ng --help
```
